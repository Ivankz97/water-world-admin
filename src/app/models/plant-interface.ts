export interface PlantInterface {
    id?: number;
    _id?: number;
    nombre?: string;
    ciudad?: string;
    telefono?: number;
    direccion?: string;
    estatus?: Boolean;
}
