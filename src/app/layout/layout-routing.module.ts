import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { RegisterCustomerComponent } from './customers/register-customer/register-customer.component';
import { RegisterEmployeeComponent } from './employees/register-employee/register-employee.component';
import { RegistrarPlantaComponent } from './plantas/registrar-planta/registrar-planta.component';
import { RegistrarSucursalComponent } from './sucursales/registrar-sucursal/registrar-sucursal.component';
import { RegistrarProductoComponent } from './productos/registrar-producto/registrar-producto.component';
import { RegisterStockComponent } from './stock/register-stock/register-stock.component';
import { RegisterUserComponent } from './users/register-user/register-user.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'register-customer', component: RegisterCustomerComponent},
            { path: 'register-employee', component: RegisterEmployeeComponent},
            { path: 'registrar-planta', component: RegistrarPlantaComponent},
            { path: 'registrar-usuario', component: RegisterUserComponent},
            { path: 'registrar-sucursal', component: RegistrarSucursalComponent},
            { path: 'registrar-producto', component: RegistrarProductoComponent},
            { path: 'registrar-inventario', component: RegisterStockComponent},
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'tipo-pagos', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
