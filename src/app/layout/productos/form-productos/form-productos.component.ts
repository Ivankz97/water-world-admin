import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from './../../../shared/services/auth.service';
import { ProductInterface } from 'src/app/models/product-interface';
import { NgForm } from '@angular/forms';
import { DataApiService } from './../../../shared/services/data-api.service';
import Swal from 'sweetalert2';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-form-productos',
  templateUrl: './form-productos.component.html',
  styleUrls: ['./form-productos.component.scss']
})
export class FormProductosComponent implements OnInit {
  @Input() product : ProductInterface = {estatus : true} as ProductInterface;
  selectedFile: File = null;
  fileToUpload: File = null;
  public imagePath;
  imgURL: any;
  headers : HttpHeaders;
  sucursalUser : string = "";
  dataUser : any = {};
  @Input() edit : boolean = false;

  constructor(
    private dataApiService: DataApiService, 
    private authService: AuthService, 
    private http: HttpClient,
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
    this.headers = new HttpHeaders({
      "Authorization": "Bearer " + this.authService.getToken(),
    });
    this.dataUser = JSON.parse(localStorage.getItem('dataUser'));
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
    console.log(event);
    //this.product.foto = this.selectedFile;
    //console.log(this.product.foto.name);
  }

  editProduct(form?: NgForm) {
    const fd = new FormData();
    if (this.selectedFile == null) {
      fd.append('nombre', this.product.nombre);
      fd.append('descripcion', this.product.descripcion);
      fd.append('precio_unitario', this.product.precio_unitario.toString());
      fd.append('unidad_de_medida', this.product.unidad_de_medida);
      fd.append('categoria', this.product.categoria);
      fd.append('sucursal', this.dataUser.sucursal.id);
      this.http.put('https://sistema.waterworld.com.mx/api/productos/' + `${this.product.id}/`, fd, {headers: this.headers})
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Editado', text: 'El producto ha sido modificado satisfactoriamente.' })
          // this.resetForm(form);
          // this.getListProducts();
          // this.edit = false;
          this.selectedFile = null;
          this.product.foto = null;
          this.imgURL = null;
          this.activeModal.close(true);
        },
          error => {
            //console.log(error);
            Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
          }
        );
    } else {
      fd.append('foto', this.selectedFile, this.selectedFile.name);
      fd.append('nombre', this.product.nombre);
      fd.append('descripcion', this.product.descripcion);
      fd.append('precio_unitario', this.product.precio_unitario.toString());
      fd.append('unidad_de_medida', this.product.unidad_de_medida);
      fd.append('categoria', this.product.categoria);
      fd.append('sucursal', this.dataUser.sucursal.id);
      this.http.put('https://sistema.waterworld.com.mx/api/productos/' + `${this.product.id}/`, fd, {headers: this.headers})
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Editado', text: 'El producto ha sido modificado satisfactoriamente.' })
          // this.resetForm(form);
          // this.getListProducts();
          // this.edit = false;
          this.selectedFile = null;
          this.product.foto = null;
          this.imgURL = null;
          this.activeModal.close(true);
        },
          error => {
            //console.log(error);
            Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
          }
        );
    }
  }

  onUpload(form?: NgForm) {
    const fd = new FormData();
    if (this.selectedFile == null) {
      fd.append('nombre', this.product.nombre);
      fd.append('descripcion', this.product.descripcion);
      fd.append('precio_unitario', this.product.precio_unitario);
      fd.append('unidad_de_medida', this.product.unidad_de_medida);
      fd.append('categoria', this.product.categoria);
      fd.append('sucursal', this.dataUser.sucursal.id);
      this.http.post('https://sistema.waterworld.com.mx/api/productos/', fd, {headers: this.headers})
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El producto ha sido registrado satisfactoriamente.' })
          // this.resetForm(form);
          // this.getListProducts();
          this.selectedFile = null;
          this.product.foto = null;
          this.imgURL = null;
          this.activeModal.close(true);
        },
          error => {
            Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
          }
        );
    } else {
      fd.append('foto', this.selectedFile, this.selectedFile.name);
      fd.append('nombre', this.product.nombre);
      fd.append('descripcion', this.product.descripcion);
      fd.append('precio_unitario', this.product.precio_unitario);
      fd.append('unidad_de_medida', this.product.unidad_de_medida);
      fd.append('categoria', this.product.categoria);
      fd.append('sucursal', this.dataUser.sucursal.id);
      this.http.post('https://sistema.waterworld.com.mx/api/productos/', fd, {headers: this.headers})
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El producto ha sido registrado satisfactoriamente.' })
          // this.resetForm(form);
          // this.getListProducts();
          this.selectedFile = null;
          this.product.foto = null;
          this.imgURL = null;
          this.activeModal.close(true);
        },
          error => {
            Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
          }
        );
    }
  }

  preview(files) {
    if (files.length === 0){
      return;
    }
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      // this.message = "Only images are supported.";
      return;
    }
    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

}
