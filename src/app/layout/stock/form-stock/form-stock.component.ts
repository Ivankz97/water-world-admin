import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataApiService } from './../../../shared/services/data-api.service';
import Swal from 'sweetalert2';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductInterface } from './../../../models/product-interface';
import { StockInterface } from './../../../models/stock-interface';

@Component({
  selector: 'app-form-stock',
  templateUrl: './form-stock.component.html',
  styleUrls: ['./form-stock.component.scss']
})
export class FormStockComponent implements OnInit {
  @Input() products   : ProductInterface[]  = [];
  @Input() sucursales : any               = [];
  @Input() inventory  : StockInterface    = {} as StockInterface;

  constructor(
    private dataApiService: DataApiService, 
    private http: HttpClient,
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
  }

  addStock(form?: NgForm) {
    if (form.value._id) {
      this.dataApiService.putProductInventory(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Editado', text: 'El inventario ha sido editado satisfactoriamente.' });
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApiService.postProductInventory(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El inventario ha sido registrado satisfactoriamente.' })
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

}