import { EmployeeInterface } from './../../../models/employee-interface';
import { BranchOfficeInterface } from './../../../models/branch-office-interface';
import { NgForm } from '@angular/forms';
import { PlantInterface } from './../../../models/plant-interface';
import { DataApiService } from './../../../shared/services/data-api.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormEmployerComponent } from '../form-employer/form-employer.component';


@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.scss']
})
export class RegisterEmployeeComponent implements OnInit {
  plants: PlantInterface;
  edit: Boolean = false;
  branchOffices: BranchOfficeInterface;
  employees: EmployeeInterface;

  //---- data table
  columns = [
    {isId:true,           show:false},
    {id:"nombre",         text:'Nombre',              show:true,  showInput:false, class:['width-152px']},
    {id:"apellidos",      text:'Apellidos',           show:true,  showInput:false},
    {id:"telefono",       text:'Teléfono',            show:true,  showInput:false},
    {id:"fechaNacimiento",text:'Fecha de nacimiento', show:true,  showInput:false},
    {id:"direccion",      text:'Dirección',           show:true,  showInput:false},
    {id:"seguroSocial",   text:'Seguro social',       show:true,  showInput:false},
    {id:"tipoEmpleado",   text:'Tipo de empleado',    show:true,  showInput:false},
    {id:"planta",         text:'Empresa',             show:true,  showInput:false},
    {id:"sucursal",       text:'Sucursal',            show:true,  showInput:false},
    {id:"fechaIngreso",   text:'Fecha de ingreso',    show:true,  showInput:false, class:['width-100px']},
    {id:"estatus",        text:'Estatus',             show:true,  showInput:false},
    {text:'Opciones',     show:true,  class:['column-top']}
  ];
  data              = [];
  dataTable         = [];
  sizeData : number = 100;
  filters           = {
    page: 1,
    limit: 10
  };
  //----

  constructor(
    private router        : Router,
    private dataApiService: DataApiService,
    private modalService  : NgbModal
  ) { }

  public employee: EmployeeInterface = {
    id: 0,
    nombre: '',
    apellidos: '',
    telefono: null,
    fechaNacimiento: null,
    direccion: null,
    seguroSocial: true,
    tipoEmpleado: '',
    planta: '',
    sucursal: '',
    fechaIngreso: null,
    estatus: ''
  };

  ngOnInit() {
    this.getListPlants();
    this.getListBranchOffices();
    this.getListEmployees();
  }

  getListPlants() {
    this.dataApiService.getAllPlants().subscribe((plants: PlantInterface) => (this.plants = plants));
  }

  getListBranchOffices() {
    this.dataApiService.getAllBranchOffices()
    .subscribe((branchOffices: BranchOfficeInterface) => (this.branchOffices = branchOffices));
  }

  openFormModal(parameter : EmployeeInterface = {} as EmployeeInterface):void{
    const modalRef = this.modalService.open(FormEmployerComponent, { centered: true, size:"lg" });
    if(Object.keys(parameter).length > 0)
      modalRef.componentInstance.employee = parameter;

    modalRef.componentInstance.plants        = this.plants;
    modalRef.componentInstance.branchOffices = this.branchOffices;

    modalRef.result.then(
      (res:any) => {
        if(typeof(res) !== 'undefined' && res)
          this.getListEmployees();
      },
      (reason:any) => { }
    )
  }

  getListEmployees() {
    this.employees = null;
    this.dataTable = [];
    this.dataApiService.getAllEmployees().subscribe((employees: EmployeeInterface) => {
      this.employees = employees;
      let listData : any = employees;
      listData.forEach((row) => {
        let buttons = [];
        if(row.estatus === 'Registrado'){
          buttons = [
            {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o',  type:'update', show:true, title:'Editar'},
            {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt',       type:'remove', show:true, title:'Borrar'}
          ]
        }else{
          buttons.push({class: 'btn btn-success btn-sm margin-between-btn margin-left-33',   text:'',  icon: 'fa fa-check', type:'active', show:true, title:'Activar registro'});
        }          
        
        let array = [
          {text:  row._id,              show:false, isID:true},
          {text:  row.nombre,           show:true},           
          {text:  row.apellidos,        show:true},           
          {text:  row.telefono,        show:true},           
          {text:  row.fechaNacimiento,  show:true},           
          {text:  row.direccion,        show:true},        
          {
            isStatus: true,
            value: row.seguroSocial,
            show:true
          },       
          {text:  row.tipoEmpleado, show:true},           
          {text:  row.planta,       show:true},           
          {text:  row.sucursal,     show:true},           
          {text:  row.fechaIngreso, show:true},           
          {
            isStatus: true,
            value: row.estatus,
            show:true
          },
          {
            isButtons:true,
            show:true,
            buttons:buttons
          }
        ];

        this.dataTable.push(array);
      });

    });
  }

  clickBtns(event){
    switch(event.type){
      case 'active':
        //TODO FALTA FUNCIÓN PARA ACTIVAR
      break;
      case 'update':
        this.openFormModal(this.employees[event.index]);
      break;
      case 'remove':
        this.deleteEmployee(this.employees[event.index]._id, this.employees[event.index]);
      break;
    }

  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.dataApiService.selectedCustomer = null;
    }
    this.edit = false;
  }

  // preEditEmployee(employee: EmployeeInterface) {
  //   this.employee = Object.assign({}, employee);
  //   window.scrollTo(0, 0);
  //   this.edit = true;
  //   console.log(this.edit);
  // }

  // addEmployee(form?: NgForm) {
  //   console.log(form.value);
  //   if (form.value._id) {
  //     this.dataApiService.putEmployee(form.value)
  //       .subscribe(res => {
  //         this.resetForm(form);
  //         this.getListEmployees();
  //         console.log('editado');
  //         Swal.fire({ type: 'success', title: 'Editado', text: 'El empleado ha sido editado satisfactoriamente.' });
  //         this.edit=false;
  //       },
  //       e => {
  //         Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
  //       });
  //   } else {
  //     this.dataApiService.postEmployee(form.value)
  //       .subscribe(res => {
  //         Swal.fire({ type: 'success', title: 'Registro', text: 'El empleado ha sido registrado satisfactoriamente.' })
  //         this.resetForm(form);
  //         this.getListEmployees();
  //       },
  //       e => {
  //         Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
  //       });
  //   }
  // }

  deleteEmployee(_id: number, form: NgForm) {
    if(confirm('Are you sure you want to delete it?')) {
      this.dataApiService.deleteEmployee(_id)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Eliminado', text: 'El empleado ha sido eliminado satisfactoriamente.' })
          this.getListEmployees();
          console.log('borrado');
        });
    }
  }

}
