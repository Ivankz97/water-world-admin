import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomerInterface } from './../../../models/customer-interface';
import { DataApiService } from './../../../shared/services/data-api.service';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-customers',
  templateUrl: './form-customers.component.html',
  styleUrls: ['./form-customers.component.scss']
})
export class FormCustomersComponent implements OnInit {
  @Input() customer : CustomerInterface = {estatus : true} as CustomerInterface;

  constructor(
    public activeModal: NgbActiveModal,
    private dataApi   : DataApiService,
  ) { }

  ngOnInit() {
  }

  addCustomer(form?: NgForm) {
    if(!form.valid){
      Swal.fire({ type: 'warning', title: 'Faltan ingresar campos requeridos'})
      return;
    }

    if (form.value._id) {
      this.dataApi.putCustomer(form.value)
        .subscribe(res => {
          // this.resetForm(form);
          // this.getListCustomers();
          console.log("editado");
          Swal.fire({ type: 'success', title: 'Editar', text: 'El cliente ha sido editado satisfactoriamente.' })
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApi.postCustomer(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El cliente ha sido registrado satisfactoriamente.' })
          // this.resetForm(form);
          // this.getListCustomers();
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

}
