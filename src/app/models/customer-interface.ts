export interface CustomerInterface {
    id?: number;
    _id?: number;
    nombre?: string;
    contacto?: string;
    zona?: string;
    telefono?: number;
    estatus?: Boolean;
}
