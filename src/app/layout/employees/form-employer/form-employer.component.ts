import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeInterface } from './../../../models/employee-interface';
import { PlantInterface } from './../../../models/plant-interface';
import { BranchOfficeInterface } from './../../../models/branch-office-interface';
import { DataApiService } from './../../../shared/services/data-api.service';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-employer',
  templateUrl: './form-employer.component.html',
  styleUrls: ['./form-employer.component.scss']
})
export class FormEmployerComponent implements OnInit {
  @Input() employee       : EmployeeInterface       = { estatus : 'Registrado' } as EmployeeInterface;
  @Input() plants         : PlantInterface[]        = [];
  @Input() branchOffices  : BranchOfficeInterface[] = [];

  constructor(
    public activeModal: NgbActiveModal,
    private dataApi   : DataApiService
  ) { }

  ngOnInit() {
  }

  addEmployee(form?: NgForm) {
    if(!form.valid){
      Swal.fire({ type: 'warning', title: 'Faltan ingresar campos requeridos'})
      return;
    }

    if (form.value._id) {
      this.dataApi.putEmployee(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Editado', text: 'El empleado ha sido editado satisfactoriamente.' });
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApi.postEmployee(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El empleado ha sido registrado satisfactoriamente.' })
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

}
