export interface BranchOfficeInterface {
    id?: number;
    _id?: number;
    planta?: string;
    direccion?: string;
    codigoPostal?: number;
    ciudad?: string;
    telefono?: number;
}
