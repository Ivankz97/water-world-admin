import { DataApiService } from './../../../shared/services/data-api.service';
import { NgForm } from '@angular/forms';
import { PlantInterface } from './../../../models/plant-interface';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormPlantasComponent } from '../form-plantas/form-plantas.component';

@Component({
  selector: 'app-registrar-planta',
  templateUrl: './registrar-planta.component.html',
  styleUrls: ['./registrar-planta.component.scss']
})
export class RegistrarPlantaComponent implements OnInit {
  plants: PlantInterface;
  edit: Boolean = false;

  //---- data table
  columns = [
    {isId:true,           show:false},
    {id:"nombre",         text:'Nombre',   show:true,  showInput:false, class:['width-152px']},
    {id:"direccion",      text:'Dirección',show:true,  showInput:false},
    {id:"ciudad",         text:'Ciudad',   show:true,  showInput:false},
    {id:"telefono",       text:'Teléfono', show:true,  showInput:false},
    {id:"estatus",        text:'Estatus',  show:true,  showInput:false},
    {text:'Opciones',     show:true,  class:['column-top']}
  ];
  data              = [];
  dataTable         = [];
  sizeData : number = 100;
  filters           = {
    page: 1,
    limit: 10
  };
  //----

  constructor(
    private router: Router,
    private dataApiService: DataApiService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.getListPlants();
  }

  getListPlants() {
    this.plants     = null;
    this.dataTable  = [];
    this.dataApiService.getAllPlants().subscribe((plants: PlantInterface) => {
      this.plants = plants;

      let listData : any = plants;
      listData.forEach((row) => {
        let buttons = [];
        if(row.estatus){
          buttons = [
            {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o',  type:'update', show:true, title:'Editar'},
            {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt',       type:'remove', show:true, title:'Borrar'}
          ]
        }else{
          buttons.push({class: 'btn btn-success btn-sm margin-between-btn margin-left-33',   text:'',  icon: 'fa fa-check', type:'active', show:true, title:'Activar registro'});
        }          
        
        let array = [
          {text:  row.id,         show:false, isID:true},
          {text:  row.nombre,     show:true},
          {text:  row.direccion,  show:true},
          {text:  row.ciudad,     show:true},
          {text:  row.telefono,   show:true},
          {
            isStatus: true,
            value: row.estatus,
            show:true
          },
          {
            isButtons:true,
            show:true,
            buttons:buttons
          }
        ];

        this.dataTable.push(array);
      });
    });
  }

  openFormModal(parameter : PlantInterface = {} as PlantInterface):void{
    const modalRef = this.modalService.open(FormPlantasComponent, { centered: true, size:"lg" });
    if(Object.keys(parameter).length > 0)
      modalRef.componentInstance.plant = parameter;

    modalRef.result.then(
      (res:any) => {
        if(typeof(res) !== 'undefined' && res)
          this.getListPlants();
      },
      (reason:any) => { }
    )
  }

  clickBtns(event){
    switch(event.type){
      case 'active':
        //TODO FALTA FUNCIÓN PARA ACTIVAR
        //this.remove(this.data[event.index]._id, true);
      break;
      case 'update':
        this.openFormModal(this.plants[event.index]);
      break;
      case 'remove':
        this.deletePlant(this.plants[event.index]._id, this.plants[event.index]);
      break;
    }
  }

  deletePlant(_id: number, form: NgForm) {
    if(confirm('Are you sure you want to delete it?')) {
      this.dataApiService.deletePlant(_id)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Eliminada', text: 'La planta ha sido eliminada satisfactoriamente.' })
          this.getListPlants();
          console.log("borrado");
        },
        e => {
          Swal.fire({ type: 'error', title: 'Error', text: 'Verifique que la planta no tenga sucursales antes de borrar.' })
        });
    }
  }

}
