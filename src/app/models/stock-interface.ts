export interface StockInterface {
    id?: number;
    _id?: number;
    producto?: number;
    descripcion?: string;
    precio_unitario?: number;
    entrada?: number;
    salida?: number;
    observaciones?: string;
    stock?: number;
}
