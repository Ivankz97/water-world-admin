import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbPaginationModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { RegisterCustomerComponent } from './customers/register-customer/register-customer.component';
import { RegisterEmployeeComponent } from './employees/register-employee/register-employee.component';
import { RegistrarPlantaComponent } from './plantas/registrar-planta/registrar-planta.component';
import { RegistrarSucursalComponent } from './sucursales/registrar-sucursal/registrar-sucursal.component';
import { RegistrarProductoComponent } from './productos/registrar-producto/registrar-producto.component';
import { RegisterStockComponent } from './stock/register-stock/register-stock.component';
import { RegisterUserComponent } from './users/register-user/register-user.component';
import { DatatableComponent } from './components/datatable/datatable.component';
import { FormCustomersComponent } from './customers/form-customers/form-customers.component';
import { FormEmployerComponent } from './employees/form-employer/form-employer.component';
import { FormPlantasComponent } from './plantas/form-plantas/form-plantas.component';
import { FormSucursalComponent } from './sucursales/form-sucursal/form-sucursal.component';
import { FormProductosComponent } from './productos/form-productos/form-productos.component';
import { FormStockComponent } from './stock/form-stock/form-stock.component';


@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        FormsModule,
        NgbPaginationModule,
        NgbModalModule
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, RegisterCustomerComponent, RegisterEmployeeComponent, RegistrarPlantaComponent, RegistrarSucursalComponent, RegistrarProductoComponent, RegisterStockComponent, RegisterUserComponent, DatatableComponent, FormCustomersComponent, FormEmployerComponent, FormPlantasComponent, FormSucursalComponent, FormProductosComponent, FormStockComponent],
    entryComponents:[
        FormCustomersComponent,
        FormEmployerComponent,
        FormPlantasComponent,
        FormSucursalComponent,
        FormProductosComponent,
        FormStockComponent
    ]
})
export class LayoutModule {}
