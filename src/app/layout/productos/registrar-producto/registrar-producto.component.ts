import { AuthService } from './../../../shared/services/auth.service';
import { ProductInterface } from 'src/app/models/product-interface';
import { NgForm } from '@angular/forms';
import { DataApiService } from './../../../shared/services/data-api.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormProductosComponent } from '../form-productos/form-productos.component';

@Component({
  selector: 'app-registrar-producto',
  templateUrl: './registrar-producto.component.html',
  styleUrls: ['./registrar-producto.component.scss']
})
export class RegistrarProductoComponent implements OnInit {
  // edit: Boolean = false;
  products: ProductInterface;
  // selectedFile: File = null;
  // fileToUpload: File = null;
  // public imagePath;
  // imgURL: any;
  // public message: string;

  //---- data table
  columns = [
    {id:"id",               text:'ID',                show:true},
    {id:"nombre",           text:'Nombre',            show:true,  showInput:false, class:['width-152px']},
    {id:"descripcion",      text:'Descripción',       show:true,  showInput:false},
    {id:"precio_unitario",  text:'Precio unitario',   show:true,  showInput:false},
    {id:"unidad_de_medida", text:'Unidad de medida',  show:true,  showInput:false},
    {id:"categoria",        text:'Categoría',         show:true,  showInput:false},
    {id:"estatus",          text:'Estatus',           show:true,  showInput:false},
    {text:'Opciones',       show:true,                class:['column-top']}
  ];
  data              = [];
  dataTable         = [];
  sizeData : number = 100;
  filters           = {
    page: 1,
    limit: 10
  };
  //----

  constructor(
    private dataApiService: DataApiService, 
    private modalService: NgbModal) { }

  // headers : HttpHeaders = new HttpHeaders({
  //   "Authorization": "Bearer " + this.authService.getToken(),
  // });

  // public product: ProductInterface = {
  //   id: 0,
  //   nombre: '',
  //   descripcion: '',
  //   precio_unitario: '',
  //   unidad_de_medida: '',
  //   categoria: '',
  //   foto: null
  // };

  ngOnInit() {
    this.getListProducts();
  }

  getListProducts() {
    this.products   = null;
    this.dataTable  = [];
    this.dataApiService.getAllProducts().subscribe((products: ProductInterface) => {
      this.products = products

      let listData : any = products;
      listData.forEach((row) => {
        let buttons = [
          {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o',  type:'update', show:true, title:'Editar'},
          {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt',       type:'remove', show:true, title:'Borrar'}
        ]
        // if(row.estatus){
        //   buttons = [
        //     {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o',  type:'update', show:true, title:'Editar'},
        //     {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt',       type:'remove', show:true, title:'Borrar'}
        //   ]
        // }else{
        //   buttons.push({class: 'btn btn-success btn-sm margin-between-btn margin-left-33',   text:'',  icon: 'fa fa-check', type:'active', show:true, title:'Activar registro'});
        // }          
        
        let array = [
          {text:  row.id,                 show:true, isID:true},
          {text:  row.nombre,             show:true},
          {text:  row.descripcion,        show:true},
          {text:  row.precio_unitario,    show:true},
          {text:  row.unidad_de_medida,   show:true},
          {text:  row.categoria,          show:true},
          {
            isStatus: true,
            value: true,
            show:true
          },
          {
            isButtons:true,
            show:true,
            buttons:buttons
          }
        ];

        this.dataTable.push(array);
      });
    });
  }

  openFormModal(parameter : ProductInterface = {} as ProductInterface):void{
    const modalRef = this.modalService.open(FormProductosComponent, { centered: true, size:"lg" });
    if(Object.keys(parameter).length > 0){
      modalRef.componentInstance.product = parameter;
      modalRef.componentInstance.edit = true;
    }

    modalRef.result.then(
      (res:any) => {
        if(typeof(res) !== 'undefined' && res)
          this.getListProducts();
      },
      (reason:any) => { }
    )
  };

  clickBtns(event){
    switch(event.type){
      case 'active':
        //TODO FALTA FUNCIÓN PARA ACTIVAR
        //this.remove(this.data[event.index]._id, true);
      break;
      case 'update':
        this.openFormModal(this.products[event.index]);
      break;
      case 'remove':
        this.deleteProduct(this.products[event.index]._id, this.products[event.index]);
      break;
    }
  };

  // resetForm(form?: NgForm) {
  //   if (form) {
  //     form.reset();
  //     this.dataApiService.selectedCustomer = null;
  //   }
  //   this.edit=false;
  //   this.imgURL = null;
  // }

  // preEditProduct(product: ProductInterface) {
  //   this.product = Object.assign({}, product);
  //   window.scrollTo(0, 0);
  //   this.edit = true;
  //   console.log(this.edit);
  //   this.imgURL = null;
  // }

  deleteProduct(_id: number, form: NgForm) {
    if (confirm('Are you sure you want to delete it?')) {
      this.dataApiService.deleteProduct(_id)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Eliminado', text: 'El producto ha sido eliminado satisfactoriamente.' })
          this.getListProducts();
          console.log('borrado');
        }, e => {
          Swal.fire({ type: 'error', title: 'Error', text: 'Verifique que este producto no se encuentre en el inventario.' })
        });
    }
  }
}
