export interface EmployeeInterface {
    id?: number;
    _id?: number;
    nombre?: string;
    apellidos?: string;
    telefono?: number;
    fechaNacimiento?: Date;
    direccion?: string;
    seguroSocial?: Boolean;
    tipoEmpleado?: string;
    planta?: string;
    sucursal?: string;
    fechaIngreso?: Date;
    estatus?: string;
}
