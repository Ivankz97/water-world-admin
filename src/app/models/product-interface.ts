export interface ProductInterface {
    id?: number;
    _id?: number;
    nombre?: string;
    descripcion?: string;
    precio_unitario?: string;
    unidad_de_medida?: string;
    categoria?: string;
    foto?: File;
}
