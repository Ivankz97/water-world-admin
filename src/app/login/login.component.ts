import { UserInterface } from './../models/user-interface';
import { AuthService } from './../shared/services/auth.service';
import { DataApiService } from './../shared/services/data-api.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(
        public router: Router,
        private authService: AuthService,
        private _dataApiService : DataApiService
    ) { }

    public user: UserInterface = {
        username: '',
        password: ''
    };

    ngOnInit() { }

    /*onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }*/

    setValuesUser():Promise<any>{
        return new Promise((resolve, reject) => {
            this._dataApiService.getAllBranchOffices().subscribe(response => {
                let objUser : any = {
                    sucursal : response[0]
                };
                localStorage.setItem('dataUser', JSON.stringify(objUser));
                resolve();
            },(error:HttpErrorResponse) => {
                reject();
            });
        });
    }


    onLogin() {
        return this.authService.loginUser(this.user.username, this.user.password).subscribe(result => {
            let token = result.access;
            this.authService.setToken(token);
            this._dataApiService.setTokenHeader(token);

            this.setValuesUser().then((data) => {
                this.router.navigate(["/dashboard"]);
            }).catch((error) => {

            });
        },
            error => Swal.fire({ type: 'error', 
            title: 'Error al iniciar sesión', text: 'El nombre de usuario o la contraseña son incorrectos.' })
        );
    }
}
