import { CustomerInterface } from './../../../models/customer-interface';
import { DataApiService } from './../../../shared/services/data-api.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormCustomersComponent } from '../form-customers/form-customers.component';

@Component({
  selector: 'app-register-customer',
  templateUrl: './register-customer.component.html',
  styleUrls: ['./register-customer.component.scss']
})
export class RegisterCustomerComponent implements OnInit {
  customers: CustomerInterface;
  edit: Boolean = false;

  constructor(
    private router: Router, 
    private dataApi: DataApiService,
    private modalService: NgbModal
    ) { }

  

  //---- data table
  columns = [
    {isId:true,           show:false},
    {id:"numInscripcion", text:'Nombre',   show:true,  showInput:false, class:['width-152px']},
    {id:"razonSocial",    text:'Contacto', show:true,  showInput:false},
    {id:"ubicacion",      text:'Zona',     show:true,  showInput:false},
    {id:"telefono",       text:'Teléfono', show:true,  showInput:false},
    {id:"estatus",        text:'Estatus',  show:true,  showInput:false},
    {text:'Opciones',     show:true,  class:['column-top']}
  ];
  data              = [];
  dataTable         = [];
  sizeData : number = 100;
  filters           = {
    page: 1,
    limit: 10
  };
  //----

  ngOnInit() {
    this.getListCustomers();
  }

  openFormModal(customer : CustomerInterface = {} as CustomerInterface):void{
    const modalRef = this.modalService.open(FormCustomersComponent, { centered: true, size:"lg" });
    if(Object.keys(customer).length > 0)
      modalRef.componentInstance.customer = customer;

    modalRef.result.then(
      (res:any) => {
        if(typeof(res) !== 'undefined' && res)
          this.getListCustomers();
      },
      (reason:any) => { }
    )
  }


  getListCustomers() {
    this.customers = null;
    this.dataTable = [];
    this.dataApi.getAllCustomers().subscribe((customers : CustomerInterface) => {
      this.customers = customers;
      let listCutomers : any = customers;
      listCutomers.forEach((row) => {
        let buttons = [];
        if(row.estatus){
          buttons = [
            {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o', type:'update', show:true, title:'Editar'},
            {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt', type:'remove', show:true, title:'Borrar'}
          ]
        }else{
          buttons.push({class: 'btn btn-success btn-sm margin-between-btn margin-left-33',   text:'',  icon: 'fa fa-check', type:'active', show:true, title:'Activar registro'});
        }          
        
        let array = [
          {text:  row._id,       show:false, isID:true},
          {text:  row.nombre,    show:true},           
          {text:  row.contacto,  show:true},           
          {text:  row.zona,      show:true},           
          {text:  row.telefono,  show:true},           
          {
            isStatus: true,
            value: row.estatus,
            show:true
          },
          {
            isButtons:true,
            show:true,
            buttons:buttons
          }
        ];

        this.dataTable.push(array);
      });


    });
  }


  clickBtns(event){
    switch(event.type){
      case 'active':
        //TODO FALTA FUNCIÓN PARA ACTIVAR
        //this.remove(this.data[event.index]._id, true);
      break;
      case 'update':
        this.openFormModal(this.customers[event.index]);
        // this.preEditCustomer(this.customers[event.index]);
      break;
      case 'remove':
        this.deleteEmployee(this.customers[event.index]._id, this.customers[event.index]);
      break;
    }

  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.dataApi.selectedCustomer = null;
    }
    this.edit = false;
  }

  preEditCustomer(customer: CustomerInterface) {
    // this.customer = Object.assign({}, customer);
    // window.scrollTo(0, 0);
    this.edit = true;
  }

  addCustomer(form?: NgForm) {
    console.log(form.value);
    if (form.value._id) {
      this.dataApi.putCustomer(form.value)
        .subscribe(res => {
          this.resetForm(form);
          this.getListCustomers();
          console.log("editado");
          Swal.fire({ type: 'success', title: 'Editar', text: 'El cliente ha sido editado satisfactoriamente.' })
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApi.postCustomer(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El cliente ha sido registrado satisfactoriamente.' })
          this.resetForm(form);
          this.getListCustomers();
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

  deleteEmployee(_id: number, form: NgForm) {
    if(confirm('Are you sure you want to delete it?')) {
      this.dataApi.deleteCustomer(_id)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Eliminado', text: 'El cliente ha sido eliminado satisfactoriamente.' })
          this.getListCustomers();
          console.log("borrado");
        });
    }
  }

}
