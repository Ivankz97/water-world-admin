import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit {
  @Input() columns    = [];
  @Input() data       = [];
  @Input() page       = 1;
  @Input() sizeData   = 0;
  @Output() clickBtns = new EventEmitter();
  @Output() filters   = new EventEmitter();
  selectedItemSize  : number    = 10;
  sizesShowData     : number[]  = [
    10,
    20,
    50
  ];

  constructor() { }

  ngOnInit() {
  }

  clickBtn(btn, index){
    this.clickBtns.emit({type: btn.type, index: index});
  }

  onKey(event: KeyboardEvent, value){    
    if(event.keyCode === 13){      
      this.filters.emit({id:value.id, value: value.value});
    }
  }

  changeSelect(value){
    this.filters.emit({id:value.id, value: value.value});
  }

  changePage(){
    this.filters.emit({id:"page", value: this.page});
  }

  onChangeSizeData(event){
    this.filters.emit({id:"limit", value: event});
  }

}
