import { UserInterface } from './../../models/user-interface';
import { AuthService } from './auth.service';
import { StockInterface } from './../../models/stock-interface';
import { EmployeeInterface } from './../../models/employee-interface';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs/internal/Observable";
import { map } from "rxjs/operators";
import { CustomerInterface } from 'src/app/models/customer-interface';
import { PlantInterface } from 'src/app/models/plant-interface';
import { ProductInterface } from 'src/app/models/product-interface';
import { PricesInterface } from 'src/app/models/prices-interface';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private http: HttpClient, private authService: AuthService) { }
  customers: Observable<any>;
  customer: Observable<any>;
  property: Observable<any>;

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    "Authorization": "Bearer " + this.authService.getToken(),
  });

  public selectedCustomer: CustomerInterface = {
    _id: null,
    nombre: "",
    contacto: "",
    zona: "",
    telefono: null,
    estatus: false
  };

  url_api = "https://sistema.waterworld.com.mx/api/clientes/";

  setTokenHeader(token:string):void{
    this.headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    });
  }

  getAllCustomers(){
    const url_api = "https://sistema.waterworld.com.mx/api/clientes/";
    return this.http.get(url_api, {headers: this.headers});
  }

  /*************************  RUTAS PARA ClIENTES  *******************************/
  registerCustomer(nombre: string, contacto: string, zona: string, telefono: number, estatus: Boolean){
    const url_api = "https://sistema.waterworld.com.mx/api/clientes/";
    return this.http.post<CustomerInterface>(url_api, {
      nombre: nombre,
      contacto: contacto,
      zona: zona,
      telefono: telefono,
      estatus: estatus
    }, {headers: this.headers}
    ).pipe(map(data=>data));
  }

  postCustomer(customer: CustomerInterface) {
    return this.http.post(this.url_api, customer, {headers: this.headers});
  }

  putCustomer(customer: CustomerInterface) {
    return this.http.put(this.url_api + `${customer._id}/`, customer, {headers: this.headers});
  }

  deleteCustomer(_id: number) {
    return this.http.delete(this.url_api + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA PLANTAS  *******************************/
  getAllPlants(){
    const url_api_plants = "https://sistema.waterworld.com.mx/api/plantas/";
    return this.http.get(url_api_plants, {headers: this.headers});
  }

  postPlant(plant: PlantInterface){
    const url_api_plants = "https://sistema.waterworld.com.mx/api/plantas/";
    return this.http.post(url_api_plants, plant, {headers: this.headers});
  }

  putPlant(plant: PlantInterface) {
    const url_api_plants = "https://sistema.waterworld.com.mx/api/plantas/";
    return this.http.put(url_api_plants + `${plant._id}/`, plant, {headers: this.headers});
  }

  deletePlant(_id: number) {
    const url_api_plants = "https://sistema.waterworld.com.mx/api/plantas/";
    return this.http.delete(url_api_plants + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA SUCURSALES  *******************************/
  getAllBranchOffices(){
    const url_api_branch_offices = "https://sistema.waterworld.com.mx/api/sucursales/";
    return this.http.get(url_api_branch_offices, {headers: this.headers});
  }

  postBranchOffice(plant: PlantInterface){
    const url_api_branch_offices = "https://sistema.waterworld.com.mx/api/sucursales/";
    return this.http.post(url_api_branch_offices, plant, {headers: this.headers});
  }

  putBranchOfffice(plant: PlantInterface) {
    const url_api_branch_offices = "https://sistema.waterworld.com.mx/api/sucursales/";
    return this.http.put(url_api_branch_offices + `${plant._id}/`, plant, {headers: this.headers});
  }

  deleteBranchOffice(_id: number) {
    const url_api_branch_offices = "https://sistema.waterworld.com.mx/api/sucursales/";
    return this.http.delete(url_api_branch_offices + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA EMPLEADOS  *******************************/
  getAllEmployees(){
    const url_api_employees = "https://sistema.waterworld.com.mx/api/empleados/";
    return this.http.get(url_api_employees, {headers: this.headers});
  }

  postEmployee(employee: EmployeeInterface){
    const url_api_employees = "https://sistema.waterworld.com.mx/api/empleados/";
    return this.http.post(url_api_employees, employee, {headers: this.headers});
  }

  putEmployee(employee: EmployeeInterface) {
    const url_api_employees = "https://sistema.waterworld.com.mx/api/empleados/";
    return this.http.put(url_api_employees + `${employee._id}/`, employee, {headers: this.headers});
  }

  deleteEmployee(_id: number) {
    const url_api_employees = "https://sistema.waterworld.com.mx/api/empleados/";
    return this.http.delete(url_api_employees + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA PLANTAS  *******************************/
  getAllUsers(){
    const url_api_users = "https://sistema.waterworld.com.mx/api/auth/register/";
    return this.http.get(url_api_users, {headers: this.headers});
  }

  postUser(user: UserInterface){
    const url_api_users = "https://sistema.waterworld.com.mx/api/auth/register/";
    return this.http.post(url_api_users, user, {headers: this.headers});
  }

  putUser(user: UserInterface) {
    const url_api_users = "https://sistema.waterworld.com.mx/api/auth/register/";
    return this.http.put(url_api_users + `${user._id}/`, user, {headers: this.headers});
  }

  deleteUser(_id: number) {
    const url_api_users = "https://sistema.waterworld.com.mx/api/auth/register/";
    return this.http.delete(url_api_users + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA PRODUCTOS  *******************************/
  getAllProducts(){
    const url_api_products = "https://sistema.waterworld.com.mx/api/productos/";
    return this.http.get(url_api_products, {headers: this.headers});
  }

  postProduct(product: ProductInterface){
    const url_api_products = "https://sistema.waterworld.com.mx/api/productos/";
    return this.http.post(url_api_products, product, {headers: this.headers});
  }

  putProduct(product: ProductInterface) {
    const url_api_products = "https://sistema.waterworld.com.mx/api/productos/";
    return this.http.put(url_api_products + `${product._id}/`, product, {headers: this.headers});
  }

  deleteProduct(_id: number) {
    const url_api_products = "https://sistema.waterworld.com.mx/api/productos/";
    return this.http.delete(url_api_products + `${_id}/`, {headers: this.headers});
  }


  /*************************  RUTAS PARA INVENTARIO  *******************************/
  getAllProductsInventory(){
    const url_api_stock = "https://sistema.waterworld.com.mx/api/inventario/";
    return this.http.get(url_api_stock, {headers: this.headers});
  }

  postProductInventory(stock: StockInterface){
    const url_api_stock = "https://sistema.waterworld.com.mx/api/inventario/";
    return this.http.post(url_api_stock, stock, {headers: this.headers});
  }

  putProductInventory(stock: StockInterface) {
    const url_api_stock = "https://sistema.waterworld.com.mx/api/inventario/";
    return this.http.put(url_api_stock + `${stock._id}/`, stock, {headers: this.headers});
  }

  deleteProductInventory(_id: number) {
    const url_api_stock = "https://sistema.waterworld.com.mx/api/inventario/";
    return this.http.delete(url_api_stock + `${_id}/`, {headers: this.headers});
  }

  /*************************  RUTAS PARA VALOR DE CREDITOS *******************************/
  getAllPrices() {
    const url_api_stock = "https://sistema.waterworld.com.mx/api/tipopago/";
    return this.http.get(url_api_stock, {headers: this.headers});
  }

  postValores(valor_creditos: number, dolar: number) {
    const url_api_watercard = "https://sistema.waterworld.com.mx/api/tipopago/";
    //return this.http.put(url_api_watercard + `${_id}/`, {headers: this.headers});
    return this.http.post(url_api_watercard, {
      valor_creditos: valor_creditos,
      dolar: dolar,
    }, {headers: this.headers}
    ).pipe(map(data => data));
  }

  putValores(precio: PricesInterface) {
    const url_api_watercard = "http://localhost:8000/api/tipopago/";
    return this.http.put(url_api_watercard + `${precio._id}/`, precio, {headers: this.headers});
  }

}
