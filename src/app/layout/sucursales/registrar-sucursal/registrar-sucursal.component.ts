import { BranchOfficeInterface } from './../../../models/branch-office-interface';
import { NgForm } from '@angular/forms';
import { PlantInterface } from './../../../models/plant-interface';
import { DataApiService } from './../../../shared/services/data-api.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormSucursalComponent } from '../form-sucursal/form-sucursal.component';

@Component({
  selector: 'app-registrar-sucursal',
  templateUrl: './registrar-sucursal.component.html',
  styleUrls: ['./registrar-sucursal.component.scss']
})
export class RegistrarSucursalComponent implements OnInit {
  plants: PlantInterface;
  edit: Boolean = false;
  branchOffices: BranchOfficeInterface;

  //---- data table
  columns = [
    {isId:true,           show:false},
    {id:"planta",         text:'Planta perteneciente',    show:true,  showInput:false, class:['width-152px']},
    {id:"direccion",      text:'Dirección',               show:true,  showInput:false},
    {id:"codigoPostal",   text:'Código postal',           show:true,  showInput:false},
    {id:"ciudad",         text:'Ciudad',                  show:true,  showInput:false},
    {id:"telefono",       text:'Teléfono',                show:true,  showInput:false},
    {id:"estatus",        text:'Estatus',                 show:true,  showInput:false},
    {text:'Opciones',     show:true,  class:['column-top']}
  ];
  data              = [];
  dataTable         = [];
  sizeData : number = 100;
  filters           = {
    page: 1,
    limit: 10
  };
  //----

  constructor(
    private router: Router, 
    private dataApiService: DataApiService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getListPlants();
  }

  getListPlants() {
    this.dataApiService.getAllPlants().subscribe((plants: PlantInterface) => {
      this.plants = plants;
      this.getListBranchOffices();
    });
  }

  getListBranchOffices() {
    this.branchOffices    = null;
    this.dataTable        = [];
    let listPlants : any  = this.plants;

    this.dataApiService.getAllBranchOffices().subscribe((branchOffices: BranchOfficeInterface) => {
      this.branchOffices = branchOffices;

      let listData : any = branchOffices;
      listData.forEach((row) => {
        let buttons = [];
        if(row.estatus){
          buttons = [
            {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o',  type:'update', show:true, title:'Editar'},
            {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt',       type:'remove', show:true, title:'Borrar'}
          ]
        }else{
          buttons.push({class: 'btn btn-success btn-sm margin-between-btn margin-left-33',   text:'',  icon: 'fa fa-check', type:'active', show:true, title:'Activar registro'});
        }          
        
        let plant : string = listPlants.find(x => x.id === row.planta).nombre;
        let array = [
          {text:  row.id,         show:false, isID:true},
          {text:  plant,          show:true},
          {text:  row.direccion,  show:true},
          {text:  row.codigoPostal,  show:true},
          {text:  row.ciudad,     show:true},
          {text:  row.telefono,   show:true},
          {
            isStatus: true,
            value: typeof(row.estatus) !== 'undefined' ? row.estatus : true,
            show:true
          },
          {
            isButtons:true,
            show:true,
            buttons:buttons
          }
        ];

        this.dataTable.push(array);
      });
    });
  }

  openFormModal(parameter : PlantInterface = {} as PlantInterface):void{
    const modalRef = this.modalService.open(FormSucursalComponent, { centered: true, size:"lg" });
    if(Object.keys(parameter).length > 0)
      modalRef.componentInstance.branchOffice = parameter;
    
    modalRef.componentInstance.plants = this.plants;

    modalRef.result.then(
      (res:any) => {
        if(typeof(res) !== 'undefined' && res)
          this.getListPlants();
      },
      (reason:any) => { }
    );
  }

  clickBtns(event){
    switch(event.type){
      case 'active':
        //TODO FALTA FUNCIÓN PARA ACTIVAR
        //this.remove(this.data[event.index]._id, true);
      break;
      case 'update':
        this.openFormModal(this.branchOffices[event.index]);
      break;
      case 'remove':
        this.deleteBranchOffice(this.branchOffices[event.index]._id, this.branchOffices[event.index]);
      break;
    }
  }

  // resetForm(form?: NgForm) {
  //   if (form) {
  //     form.reset();
  //     this.dataApiService.selectedCustomer = null;
  //   }
  //   this.edit = false;
  // }

  // preEditBranchOffice(branch_office: PlantInterface) {
  //   this.branchOffice = Object.assign({}, branch_office);
  //   window.scrollTo(0, 0);
  //   this.edit = true;
  //   console.log(this.edit);
  // }

  deleteBranchOffice(_id: number, form: NgForm) {
    if(confirm('Are you sure you want to delete it?')) {
      this.dataApiService.deleteBranchOffice(_id)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Eliminada', text: 'La sucursal ha sido eliminada satisfactoriamente.' })
          this.getListBranchOffices();
          console.log("borrado");
        });
    }
  }

}
