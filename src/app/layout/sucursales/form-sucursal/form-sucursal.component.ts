import { Component, OnInit, Input } from '@angular/core';
import { BranchOfficeInterface } from './../../../models/branch-office-interface';
import { NgForm } from '@angular/forms';
import { PlantInterface } from './../../../models/plant-interface';
import { DataApiService } from './../../../shared/services/data-api.service';
import Swal from 'sweetalert2';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-form-sucursal',
  templateUrl: './form-sucursal.component.html',
  styleUrls: ['./form-sucursal.component.scss']
})
export class FormSucursalComponent implements OnInit {
  @Input() branchOffice : BranchOfficeInterface = {estatus : true} as BranchOfficeInterface;
  @Input() plants       : PlantInterface[]      = [];

  constructor(
    private dataApiService: DataApiService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

  addBranchOffice(form?: NgForm) {
    if(!form.valid){
      Swal.fire({ type: 'warning', title: 'Faltan ingresar campos requeridos'})
      return;
    }

    if (form.value._id) {
      this.dataApiService.putBranchOfffice(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Editado', text: 'La sucursal ha sido editada satisfactoriamente.' });
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApiService.postBranchOffice(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'La sucursal ha sido registrada satisfactoriamente.' })
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

}
