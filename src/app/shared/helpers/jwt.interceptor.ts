import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService, private spinner: NgxSpinnerService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(this.authService.getToken() !== null && this.authService.getToken() !== ""){
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.authService.getToken()}`
                }
            });
        }

        this.spinner.show();
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse)
                    this.spinner.hide();

                if (event instanceof HttpErrorResponse)
                    this.spinner.hide();

                return event;
            }),
        catchError(err => {
            console.log(err);
            this.spinner.hide();
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authService.logoutUser();
                location.reload(true);
            }
            setTimeout(()=> {
                this.spinner.hide();
            },1000);
            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }
}