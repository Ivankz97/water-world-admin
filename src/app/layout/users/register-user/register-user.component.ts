import { UserInterface } from './../../../models/user-interface';
import { DataApiService } from './../../../shared/services/data-api.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {
  users: UserInterface;
  edit: Boolean = false;
  constructor(private router: Router, private dataApi: DataApiService) { }

  public user: UserInterface = {
    id: 0,
    first_name: '',
    last_name: '',
    email: '',
    username: '',
    password: '',
  };

  ngOnInit() {
    //this.getListUsers();
  }


  getListUsers() {
    this.dataApi.getAllUsers().subscribe((users: UserInterface) => (this.users = users));
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.edit=false;
    }
  }

  preEditUser(user: UserInterface) {
    this.user = Object.assign({}, user);
    window.scrollTo(0, 0);
    this.edit = true;
    console.log(this.edit);
  }

  addUser(form?: NgForm) {
    console.log(form.value);
    if (form.value._id) {
      this.dataApi.putUser(form.value)
        .subscribe(res => {
          this.resetForm(form);
          this.getListUsers();
          console.log("editado");
          Swal.fire({ type: 'success', title: 'Editar', text: 'El usuario ha sido editado satisfactoriamente.' })
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApi.postUser(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'El usuario ha sido registrado satisfactoriamente.' })
          this.resetForm(form);
          this.getListUsers();
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

  deleteUser(_id: number, form: NgForm) {
    if(confirm('Are you sure you want to delete it?')) {
      this.dataApi.deleteUser(_id)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Eliminado', text: 'El usuario ha sido eliminado satisfactoriamente.' })
          this.getListUsers();
          console.log("borrado");
        });
    }
  }
}
