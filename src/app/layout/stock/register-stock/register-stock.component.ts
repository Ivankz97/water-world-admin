import { StockInterface } from './../../../models/stock-interface';
import { ProductInterface } from 'src/app/models/product-interface';
import { NgForm } from '@angular/forms';
import { DataApiService } from './../../../shared/services/data-api.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormStockComponent } from '../form-stock/form-stock.component';

@Component({
  selector: 'app-register-stock',
  templateUrl: './register-stock.component.html',
  styleUrls: ['./register-stock.component.scss']
})
export class RegisterStockComponent implements OnInit {
  edit: Boolean = false;
  products: ProductInterface;
  sucursales : any = [];

  //---- data table
  columns = [
    {id:"id",             text:'ID',            show:true},
    {id:"producto",       text:'Producto',      show:true,  showInput:false, class:['width-152px']},
    {id:"sucursal",       text:'Sucursal',      show:true,  showInput:false},
    {id:"observaciones",  text:'Observaciones', show:true,  showInput:false},
    {id:"stock",          text:'Stock',         show:true,  showInput:false},
    {id:"estatus",        text:'Estatus',       show:true,  showInput:false},
    {text:'Opciones',       show:true,                class:['column-top']}
  ];
  data              = [];
  dataTable         = [];
  sizeData : number = 100;
  filters           = {
    page: 1,
    limit: 10
  };
  //----

  constructor(
    private router: Router, 
    private dataApiService: DataApiService,
    private modalService: NgbModal
    ) { }
  stocks: StockInterface;

  // public product: ProductInterface = {
  //   id: 0,
  //   nombre: '',
  //   descripcion: '',
  //   precio_unitario: null,
  //   unidad_de_medida: '',
  //   categoria: '',
  //   foto: null
  // };

  // public inventory: StockInterface = {
  //   id: 0,
  //   producto: null,
  //   descripcion: '',
  //   precio_unitario: null,
  //   entrada: null,
  //   salida: null,
  //   observaciones: '',
  //   stock: null
  // };

  ngOnInit() {
    this.getListProducts();
    this.getSucursales();
  }

  getListProducts() {
    this.dataApiService.getAllProducts()
      .subscribe((products: ProductInterface) => (this.products = products));
  }

  getListProductStock() {
    this.stocks     = null;
    this.dataTable  = [];
    this.dataApiService.getAllProductsInventory().subscribe((stocks: StockInterface) => {
      this.stocks = stocks;

      let listData : any = stocks;
      listData.forEach((row) => {
        let buttons = [
          {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o',  type:'update', show:true, title:'Editar'},
          {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt',       type:'remove', show:true, title:'Borrar'}
        ]
        // if(row.estatus){
        //   buttons = [
        //     {class: 'btn btn-primary btn-sm margin-between-btn',  text:'',  icon: 'fa fa-pencil-square-o',  type:'update', show:true, title:'Editar'},
        //     {class: 'btn btn-danger btn-sm margin-between-btn',   text:'',  icon: 'fas fa-trash-alt',       type:'remove', show:true, title:'Borrar'}
        //   ]
        // }else{
        //   buttons.push({class: 'btn btn-success btn-sm margin-between-btn margin-left-33',   text:'',  icon: 'fa fa-check', type:'active', show:true, title:'Activar registro'});
        // }
        
        let sucursal      = this.sucursales.find(x => x.id == row.sucursal);
        let nameSucursal  = sucursal !== null && Object.keys(sucursal).length > 0 ? `${sucursal.ciudad} ${sucursal.direccion}` : '';
        let array = [
          {text:  row.id,                 show:true, isID:true},
          {text:  row.producto.nombre,    show:true},
          {text:  nameSucursal,           show:true},
          {text:  row.observaciones,      show:true},
          {text:  row.stock,              show:true},
          {
            isStatus: true,
            value: true,
            show:true
          },
          {
            isButtons:true,
            show:true,
            buttons:buttons
          }
        ];

        this.dataTable.push(array);
      });
    });
  }

  getSucursales():void{
    this.dataApiService.getAllBranchOffices().subscribe(response => {
      this.sucursales = response;
      this.getListProductStock();

    },(error:HttpErrorResponse) => {
    });
  }

  openFormModal(parameter : StockInterface = {} as StockInterface):void{
    const modalRef = this.modalService.open(FormStockComponent, { centered: true, size:"lg" });
    if(Object.keys(parameter).length > 0){
      modalRef.componentInstance.inventory  = parameter;
    }

    modalRef.componentInstance.sucursales = this.sucursales;
    modalRef.componentInstance.products   = this.products;

    modalRef.result.then(
      (res:any) => {
        if(typeof(res) !== 'undefined' && res)
          this.getListProductStock();
      },
      (reason:any) => { }
    )
  };

  clickBtns(event){
    switch(event.type){
      case 'active':
        //TODO FALTA FUNCIÓN PARA ACTIVAR
        //this.remove(this.data[event.index]._id, true);
      break;
      case 'update':
        this.openFormModal(this.stocks[event.index]);
      break;
      case 'remove':
        this.deleteStock(this.stocks[event.index]._id, this.stocks[event.index]);
      break;
    }
  };

  // resetForm(form?: NgForm) {
  //   if (form) {
  //     form.reset();
  //     this.dataApiService.selectedCustomer = null;
  //   }
  //   this.edit = false;
  // }

  // preEditStock(inventory: StockInterface) {
  //   this.inventory = Object.assign({}, inventory);
  //   window.scrollTo(0, 0);
  //   this.edit = true;
  //   console.log(this.edit, this.inventory);
  // }

  deleteStock(_id: number, form: NgForm) {
    if(confirm('Are you sure you want to delete it?')) {
      this.dataApiService.deleteProductInventory(_id)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Eliminada', text: 'El inventario ha sido eliminado satisfactoriamente.' })
          this.getListProductStock();
          console.log("borrado");
        });
    }
  }

}
