import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { DataApiService } from '../../shared/services/data-api.service';
import { PricesInterface } from '../../models/prices-interface';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {
    edit: Boolean = false;
    valor_creditos: number = null;
    dolar: number = null;
    precios: any = [];


    public prices: PricesInterface = {
        id: 0,
        valor_creditos: null,
        dolar: null
    };
    constructor(private dataApiService: DataApiService) { }

    ngOnInit() {
        this.getPrices();
    }

    getPrices() {
        this.dataApiService.getAllPrices().subscribe(data => {
            this.precios = data, console.log(this.precios);
        });
    }

    postValores() {
        if (this.precios.length === 0 ) {
            this.dataApiService.postValores(
                this.prices.valor_creditos,
                this.prices.dolar
            ).subscribe(data => {
                Swal.fire({ type: 'success', title: 'Editado', text: 'Los precios han sido registrados satisfactoriamente.' });
                this.getPrices();
            }, error => {
                Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos.' });
            });
        }
    }

    preEditPrices(prices: PricesInterface) {
        this.prices = Object.assign({}, prices);
        window.scrollTo(0, 0);
        this.edit = true;
        console.log(this.edit);
    }

    editPrices(form?: NgForm) {
        if (form.value._id) {
            this.dataApiService.putValores(form.value)
                .subscribe(res => {
                    this.resetForm(form);
                    //this.getListUsers();
                    this.getPrices();
                    console.log("editado");
                    Swal.fire({ type: 'success', title: 'Editar', text: 'Los precios han sido ajustados satisfactoriamente.' });
                },
                    e => {
                        Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' });
                    });
        }
    }

    resetForm(form?: NgForm) {
        if (form) {
            form.reset();
            this.edit = false;
        }
    }
}
