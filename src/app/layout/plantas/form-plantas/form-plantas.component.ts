import { Component, OnInit, Input } from '@angular/core';
import { DataApiService } from './../../../shared/services/data-api.service';
import { NgForm } from '@angular/forms';
import { PlantInterface } from './../../../models/plant-interface';
import Swal from 'sweetalert2';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-form-plantas',
  templateUrl: './form-plantas.component.html',
  styleUrls: ['./form-plantas.component.scss']
})
export class FormPlantasComponent implements OnInit {
  @Input() plant : PlantInterface = {estatus : true} as PlantInterface;

  constructor(
    private dataApiService: DataApiService,
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
  }

  addPlant(form?: NgForm) {
    if(!form.valid){
      Swal.fire({ type: 'warning', title: 'Faltan ingresar campos requeridos'})
      return;
    }

    if (form.value._id) {
      this.dataApiService.putPlant(form.value)
        .subscribe(res => {
          console.log("editado");
          Swal.fire({ type: 'success', title: 'Editado', text: 'La planta ha sido editada satisfactoriamente.' });
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Edición inválida', text: 'Datos del formulario incompletos' })
        });
    } else {
      this.dataApiService.postPlant(form.value)
        .subscribe(res => {
          Swal.fire({ type: 'success', title: 'Registro', text: 'La planta ha sido registrada satisfactoriamente.' })
          this.activeModal.close(true);
        },
        e => {
          Swal.fire({ type: 'error', title: 'Registro inválido', text: 'Datos del formulario incompletos' })
        });
    }
  }

}
